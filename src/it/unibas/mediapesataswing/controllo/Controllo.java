package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.vista.Vista;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Action;

public class Controllo {
    
    private Vista vista;
    private Modello modello;
    private Map<String, Action> mappaAzioni = new HashMap<String, Action>();
    
    public Vista getVista() {
        return this.vista;
    }
    
    public Modello getModello() {
        return this.modello;
    }
    
    public Controllo() {
        this.inizializza();
        this.modello = new Modello();
        this.vista = new Vista(this, modello);
    }
    
    public void inizializza() {
        inizializzaAzioni();
        abilitazioniIniziali();
    }
    
    private void inizializzaAzioni() {
        this.mappaAzioni.put(Costanti.AZIONE_NUOVO, new AzioneNuovo(this));
        this.mappaAzioni.put(Costanti.AZIONE_APRI, new AzioneApri(this));
        this.mappaAzioni.put(Costanti.AZIONE_SALVA, new AzioneSalva(this));
        this.mappaAzioni.put(Costanti.AZIONE_ESCI, new AzioneEsci(this));
        this.mappaAzioni.put(Costanti.AZIONE_MODIFICA_STUDENTE, new AzioneFinestraModificaStudente(this));
        this.mappaAzioni.put(Costanti.AZIONE_INSERISCI_ESAME, new AzioneInserisciEsame(this));
        this.mappaAzioni.put(Costanti.AZIONE_MODIFICA_ESAME, new AzioneFinestraModificaEsame(this));
        this.mappaAzioni.put(Costanti.AZIONE_ELIMINA_ESAME, new AzioneEliminaEsame(this));
        this.mappaAzioni.put(Costanti.AZIONE_AGGIORNA_STUDENTE, new AzioneModificaStudente(this));
        this.mappaAzioni.put(Costanti.AZIONE_AGGIORNA_ESAME, new AzioneModificaEsame(this));
        this.mappaAzioni.put(Costanti.AZIONE_CALCOLA_MEDIA, new AzioneCalcolaMedia(this));
        this.mappaAzioni.put(Costanti.AZIONE_VISUALIZZA_LISTA, new AzioneVisualizzaLista(this));
        this.mappaAzioni.put(Costanti.AZIONE_VISUALIZZA_TABELLA, new AzioneVisualizzaTabella(this));
        this.mappaAzioni.put(Costanti.AZIONE_APRI_EXCEL, new AzioneApriExcel(this));
        this.mappaAzioni.put(Costanti.AZIONE_APRI_EXCEL_NO_THREAD, new AzioneApriExcelNoThread(this));
        this.mappaAzioni.put(Costanti.AZIONE_INFORMAZIONI, new AzioneFinestraInformazioni(this));
    }
    
    private void abilitazioniIniziali() {
        this.mappaAzioni.get(Costanti.AZIONE_SALVA).setEnabled(false);
        this.mappaAzioni.get(Costanti.AZIONE_MODIFICA_STUDENTE).setEnabled(false);
        this.mappaAzioni.get(Costanti.AZIONE_INSERISCI_ESAME).setEnabled(false);
        this.mappaAzioni.get(Costanti.AZIONE_MODIFICA_ESAME).setEnabled(false);
        this.mappaAzioni.get(Costanti.AZIONE_ELIMINA_ESAME).setEnabled(false);
        this.mappaAzioni.get(Costanti.AZIONE_CALCOLA_MEDIA).setEnabled(false);
        this.mappaAzioni.get(Costanti.AZIONE_VISUALIZZA_LISTA).setEnabled(false);
        this.mappaAzioni.get(Costanti.AZIONE_VISUALIZZA_TABELLA).setEnabled(false);
        this.mappaAzioni.get(Costanti.AZIONE_APRI_EXCEL).setEnabled(false);
        this.mappaAzioni.get(Costanti.AZIONE_APRI_EXCEL_NO_THREAD).setEnabled(false);
    }
    
    public Action getAzione(String nomeAzione) {
        return mappaAzioni.get(nomeAzione);
    }
    
    /* *******************************************
     *              Main
     * ********************************************/
    
    public static void main(String[] args){
        javax.swing.SwingUtilities.invokeLater(
                new Runnable() {
            public void run() {
                Controllo controllo = new Controllo();
            }
        }
        );
    }
    
}
