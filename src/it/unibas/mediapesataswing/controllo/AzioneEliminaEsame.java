package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import it.unibas.mediapesataswing.vista.PannelloPrincipale;
import it.unibas.mediapesataswing.vista.Vista;

public class AzioneEliminaEsame extends javax.swing.AbstractAction {
    
    private Controllo controllo;
    
    public AzioneEliminaEsame(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Elimina Esame");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Elimina un esame dalla lista");
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Modello modello = this.controllo.getModello();
        Vista vista = this.controllo.getVista();
        PannelloPrincipale pannello = (PannelloPrincipale)vista.getSottoVista(Costanti.VISTA_PANNELLO_PRINCIPALE); 
        int indiceSelezione = pannello.getSelectedIndex();
        if (indiceSelezione != -1) {
            Studente studente = (Studente)modello.getBean(Costanti.STUDENTE);
            studente.eliminaEsame(indiceSelezione);
            pannello.aggiornaEsami();
        } else {
            vista.finestraErrore("E' necessario selezionare un esame");
        }
        
    }
    
}
