package it.unibas.mediapesataswing.vista;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.controllo.Controllo;
import it.unibas.mediapesataswing.modello.Modello;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Vista extends javax.swing.JFrame {
    
    static {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
    }
    
    private Controllo controllo;
    private Modello modello;
    
    public Controllo getControllo() {
        return this.controllo;
    }
    
    public Modello getModello() {
        return this.modello;
    }
    
    private Map<String, Object> mappaSottoViste = new HashMap<String, Object>();
    
    public Object getSottoVista(String nome) {
        return this.mappaSottoViste.get(nome);
    }
    
    public Vista(Controllo controllo, Modello modello) {
        this.controllo = controllo;
        this.modello = modello;
        this.inizializza();
    }
        
    private JFileChooser fileChooser = new JFileChooser();
    
    public JFileChooser getFileChooser() {
        return this.fileChooser;
    }
    
    /* *******************************************
     *       Inizializzazione del frame
     * ********************************************/

    public void inizializza() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Media Pesata");
        creaMenu();
        ((JPanel)this.getContentPane()).setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        PannelloPrincipale pannello = new PannelloPrincipale(this);
        FinestraStudente finestraStudente = new FinestraStudente(this);
        FinestraEsame finestraEsame = new FinestraEsame(this); 
        this.mappaSottoViste.put(Costanti.VISTA_PANNELLO_PRINCIPALE, pannello);
        this.mappaSottoViste.put(Costanti.VISTA_FINESTRA_STUDENTE, finestraStudente);
        this.mappaSottoViste.put(Costanti.VISTA_FINESTRA_ESAME, finestraEsame);
        JScrollPane scrollPane = new JScrollPane(pannello);
        this.getContentPane().add(scrollPane);
        schermoIniziale();
    }
    
    /* *******************************************
     *       Schermi
     * ********************************************/
    
    public void schermoIniziale() {
        this.pack();
        this.setVisible(true);
    }
    
    /* *******************************************
     *       Gestione frame
     * ********************************************/
    
    private void creaMenu() {
        JMenuBar barraMenu = new JMenuBar();
        this.setJMenuBar(barraMenu);
        creaMenuFile(barraMenu);
        creaMenuModifica(barraMenu);
        creaMenuVisualizza(barraMenu);
        creaMenuHelp(barraMenu);
    }
    
    private void creaMenuFile(javax.swing.JMenuBar barraMenu) {
        JMenu menuFile = new JMenu("File");
        menuFile.setMnemonic(java.awt.event.KeyEvent.VK_F);
        barraMenu.add(menuFile);
        JMenuItem voceNuovo = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_NUOVO));
        JMenuItem voceApri = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_APRI));
        JMenuItem voceSalva = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_SALVA));
        JMenuItem voceApriExcel = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_APRI_EXCEL));
        JMenuItem voceApriExcelNoThread = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_APRI_EXCEL_NO_THREAD));
        JMenuItem voceEsci = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_ESCI));
        menuFile.add(voceNuovo);
        menuFile.addSeparator();
        menuFile.add(voceApri);
        menuFile.add(voceSalva);
        menuFile.addSeparator();
        menuFile.add(voceApriExcel);
        menuFile.add(voceApriExcelNoThread);
        menuFile.addSeparator();
        menuFile.add(voceEsci);
    }
    
    private void creaMenuModifica(javax.swing.JMenuBar barraMenu) {
        JMenu menuModifica = new JMenu("Modifica");
        menuModifica.setMnemonic(java.awt.event.KeyEvent.VK_M);
        barraMenu.add(menuModifica);
        JMenuItem voceModificaDatiStudente = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_MODIFICA_STUDENTE));
        JMenuItem voceInserisciEsame = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_INSERISCI_ESAME));
        JMenuItem voceModificaEsame = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_MODIFICA_ESAME));
        JMenuItem voceEliminaEsame = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_ELIMINA_ESAME));
        JMenuItem voceCalcolaMedia = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_CALCOLA_MEDIA));
        menuModifica.add(voceModificaDatiStudente);
        menuModifica.addSeparator();
        menuModifica.add(voceInserisciEsame);
        menuModifica.add(voceModificaEsame);
        menuModifica.add(voceEliminaEsame);
        menuModifica.addSeparator();
        menuModifica.add(voceCalcolaMedia);
    }
    
    private void creaMenuVisualizza(javax.swing.JMenuBar barraMenu) {
        JMenu menuVisualizza = new JMenu("Visualizza");
        menuVisualizza.setMnemonic(java.awt.event.KeyEvent.VK_V);
        barraMenu.add(menuVisualizza);
        JMenuItem voceVisualizzaLista = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_VISUALIZZA_LISTA));
        JMenuItem voceVisualizzaTabella = new JMenuItem(this.controllo.getAzione(Costanti.AZIONE_VISUALIZZA_TABELLA));
        menuVisualizza.add(voceVisualizzaLista);
        menuVisualizza.add(voceVisualizzaTabella);
    }
    
    private void creaMenuHelp(javax.swing.JMenuBar barraMenu) {
        javax.swing.JMenu menuHelp = new javax.swing.JMenu("?");
        menuHelp.setMnemonic(java.awt.event.KeyEvent.VK_H);
        barraMenu.add(menuHelp);
        javax.swing.JMenuItem voceAbout = new javax.swing.JMenuItem(this.controllo.getAzione(Costanti.AZIONE_INFORMAZIONI));
        menuHelp.add(voceAbout);
    }
    
    /* *******************************************
     *              Finestre di Dialogo
     * ********************************************/
    
    public void finestraModificaStudente() {
        ((FinestraStudente)this.mappaSottoViste.get(Costanti.VISTA_FINESTRA_STUDENTE)).visualizza();
    }
    
    public void finestraModificaEsame() {
        ((FinestraEsame)this.mappaSottoViste.get(Costanti.VISTA_FINESTRA_ESAME)).visualizza();
    }
    
    public void finestraAbout() {
        StringBuffer messaggio = new StringBuffer();
        messaggio.append("Media Pesata\n");
        messaggio.append("Esempio sviluppato a scopo didattico\n");
        messaggio.append("\n");
        messaggio.append("Corso di Laurea in Informatica\n");
        messaggio.append("UniversitÓ della Basilicata\n");
        messaggio.append("mecca@unibas.it\n");
        JOptionPane.showMessageDialog(this, messaggio.toString());
    }
    
    public void finestraErrore(String messaggio) {
        JOptionPane.showMessageDialog(this, messaggio, "ERRORE", JOptionPane.ERROR_MESSAGE);
    }
}
