package it.unibas.mediapesataswing.modello;

import java.util.ArrayList;

public class Studente {

    private String nome;
    private String cognome;
    private int matricola;
    private ArrayList<Esame> listaEsami = new ArrayList<Esame>();
    
    public Studente() {
        this.nome = "";
        this.cognome = "";
        this.matricola = 0;
    }
    
    public Studente (String nome, String cognome, int matricola) {
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
    }
    
    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getCognome() {
        return this.cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getMatricola() {
        return this.matricola;
    }

    public void setMatricola(int matricola){
        this.matricola = matricola;
    }
    
    public void addEsame(String insegnamento, int voto, boolean lode, int crediti) {
        Esame esame = new Esame(insegnamento, voto, lode, crediti);
        this.listaEsami.add(esame);
    }

    public void addEsame(Esame esame) {
        this.listaEsami.add(esame);
    }

    public Esame getEsame(int i) {
        if (i < 0 || i >= this.listaEsami.size()) {
            throw new IndexOutOfBoundsException("Esame inesistente");
        }
        return this.listaEsami.get(i);
    }

    public void eliminaEsame(int i) {
        if (i < 0 || i >= this.listaEsami.size()) {
            throw new IndexOutOfBoundsException("Esame inesistente");
        }
        this.listaEsami.remove(i);
    }
    
    public int getNumeroEsami() {
        return this.listaEsami.size();
    }

    public java.util.List getListaEsami() {
        return this.listaEsami;
    }
    
    public double getMediaPesata() {
        if (this.listaEsami.size() == 0) {
            throw new IllegalArgumentException("Non e' possibile calcolare la media di 0 esami");
        }
        int sommaVotiPesati = 0;
        int sommaCrediti = 0;
        for (int i = 0; i < this.listaEsami.size(); i++) {
            Esame esameIesimo = (Esame)this.listaEsami.get(i);
            sommaVotiPesati += esameIesimo.getVoto() * esameIesimo.getCrediti();
            sommaCrediti += esameIesimo.getCrediti();
        }
        return ((double)sommaVotiPesati)/sommaCrediti;
    }

    public double getMediaPartenza() {
        return getMediaPesata()/30 * 110;
    }
    
    public String toString() {
        return "Cognome: " + getCognome() + " - Nome: " + getNome()
             + " - Matricola: " + getMatricola();
    }
    
    public String toSaveString() {
        return getCognome() + " , " + getNome() + " , " + getMatricola();
    }    

}


